// import logo from './assets/logo.svg';
// import './assets/App.css';
import React, { ReactFragment } from 'react';

import {
  createBrowserRouter,
  RouterProvider
} from 'react-router-dom'

import Home from './component/home';
import Pokemon from './component/pokemon';
import Question from './component/question';

const routerApp = createBrowserRouter([
  {
    path: '/',
    element: <Home />
  },
  {
    path: '/question',
    element: <Question />
  },
  {
    path: '/pokemon',
    element: <Pokemon />
  }
])

function App() {
  return (<div>
    <RouterProvider router={routerApp} />
  </div>);
}

export default App;
