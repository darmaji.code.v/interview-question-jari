import React, { useState, useEffect, useMemo, useCallback } from "react"
import { CALL_POKEMON } from "../../api"

import PokemonCard from "./pokemonCard"

const Pokemon = () => {
  const [pokemons, setPokemons] = useState([])
  const [favoritePokemons, setFavoritePokemons] = useState([])
  const [showFavoritePokemons, setShowFavoritePokemons] = useState(false)
  
  useEffect(() => {
    const localPokemons = localStorage.getItem('pokemons')
    const localFavoritePokemons = localStorage.getItem('favoritePokemons')
    
    const haveLocalPokemons = !!localPokemons?.replace('[', '')?.replace(']', '')?.length
    if (haveLocalPokemons) { setPokemons([...JSON.parse(localPokemons)]) }
    else { callPokemons() }

    const haveLocalFavoritePokemons = !!localFavoritePokemons?.replace('[', '')?.replace(']', '')?.length
    if (haveLocalFavoritePokemons) {
      setFavoritePokemons([...JSON.parse(localFavoritePokemons)])
    }

  }, [])

  useEffect(() => {
    localStorage.setItem('favoritePokemons', JSON.stringify(favoritePokemons))
  }, [favoritePokemons])

  const callPokemons = useCallback(async () => {
    const pokemonsData = await CALL_POKEMON()

    if (pokemonsData?.status) {
      setPokemons([...pokemonsData.data.data])
      localStorage.setItem('pokemons', JSON.stringify(pokemonsData.data.data))
    }
    else {
      alert('Yah, tidak ada pokemon yang bisa dipanggil')
    }
  }, [])

  const renderListPokemon = useMemo(() => !pokemons.length ? 'Tunggu sebentar, pokemon nya lagi dijalan, sebentar lagi sampai' : pokemons.map((pokemon, index) => {
    pokemon.isFavorite = !!favoritePokemons.filter(itemFavoritePokemons => itemFavoritePokemons.id === pokemon.id).length

    return (<div key={index}>
      <PokemonCard 
        dataPokemon={pokemon}
        favoritePokemons={favoritePokemons} 
        setFavoritePokemons={setFavoritePokemons}
      />
    </div>)
  }), [pokemons, favoritePokemons])

  const renderListFavoritePokemon = useMemo(() => !favoritePokemons.length ? 'Kamu belum memilih pokemon yang kamu sukai' : favoritePokemons.map((pokemon, index) => {
    pokemon.isFavorite = !!favoritePokemons.filter(itemFavoritePokemons => itemFavoritePokemons.id === pokemon.id).length

    return (<div key={index}>
      <PokemonCard 
        dataPokemon={pokemon}
        favoritePokemons={favoritePokemons} 
        setFavoritePokemons={setFavoritePokemons}
      />
    </div>)
  }), [favoritePokemons])

  return (
    <div className="pokemon-wrapper">
      <div className="header-pokemon-page">
        <span>
          {showFavoritePokemons ? `Kamu menyukai ${favoritePokemons.length} Pokemon` : `Ada ${pokemons.length} Pokemon terpanggil`}
        </span>
        <button 
          className="button-favorite"
          onClick={() => setShowFavoritePokemons(!showFavoritePokemons)}
        >
          {showFavoritePokemons ? `Semua Pokemon` : `Favorite ${favoritePokemons.length}`}
        </button>
      </div>
      <div className="pokemon-card-wrapper">
        {showFavoritePokemons && renderListFavoritePokemon}
        {!showFavoritePokemons && renderListPokemon}
      </div>
    </div>
  )
}

export default Pokemon
