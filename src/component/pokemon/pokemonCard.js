import React, { useState, useMemo } from "react";

const PokemonCard = ({ 
  dataPokemon,
  favoritePokemons,
  setFavoritePokemons
}) => {
  const [imgLoaded, setImgLoaded] = useState(false)
  const { images, name, supertype, types, abilities } = dataPokemon

  const renderAbilitieItem = (abilitiesData) => !!abilitiesData?.length && abilitiesData.map((abilitie, index) => (<div className="abilities-box text-left" key={index}>
    <div>{abilitie.name}</div>
    <span>
      {abilitie.text}
    </span>
  </div>))

  const renderAbilities = useMemo(() => (<div className="abilities-wrapper">
    {renderAbilitieItem(abilities)}
  </div>), [abilities])
  
  const toggleFavorite = () => {
    
    if (dataPokemon.isFavorite) {
      const newFavoritePokemons = favoritePokemons.filter(itemFavoritePokemons => itemFavoritePokemons.id !== dataPokemon.id)
      
      setFavoritePokemons([...newFavoritePokemons])
    }
    else {
      setFavoritePokemons([...favoritePokemons, dataPokemon])
    }

  }

  return(<a className="pokemon-card ">
    <div className="pokemon-card-inner">
      <div className="card-front">
        <img 
          src={images?.small} 
          loading="lazy" 
          alt={name} 
          className={imgLoaded ? '' : 'opacity-0'}
          onLoad={() => setImgLoaded(true)}
        />
        <img 
          src="./loading.svg" 
          alt="loading image"
          className={imgLoaded ? 'opacity-0' : ''}
        />
      </div>
      <div className="card-back">
        <div>
          <div className="pokemon-header">
            <span>
              {name}
            </span>
            <svg 
              width="24" 
              height="24" 
              fill="currentColor" 
              className={dataPokemon.isFavorite ? 'bi bi-star-fill active' : 'bi bi-star-fill' } 
              viewBox="0 0 16 16"
              onClick={() => toggleFavorite()}
            >
              <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
            </svg>
          </div>

          <table>
            <tbody>
              <tr>
                <td valign="top" align="left" width={70}>
                  Supertype
                </td>
                <td valign="top" width={10}>:</td>
                <td align="left">
                  {supertype}
                </td>
              </tr>
              <tr>
                <td valign="top" align="left">
                  Type
                </td>
                <td valign="top">:</td>
                <td align="left">
                  {types?.join(', ')}
                </td>
              </tr>
            </tbody>
          </table>

          {!!abilities?.length && renderAbilities}

        </div>
      </div>
    </div>
  </a>)
}

export default PokemonCard
