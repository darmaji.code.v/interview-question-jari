import { Link } from "react-router-dom"

const Navbar = () => {
  return (<nav>
    <div>
      <a href="/" className="navbar-logo border-box">
        TEST
      </a>
      <ol>
        <li className="border-box">
          <a href="/pokemon">
            Pokemon
          </a>
        </li>
        <li className="border-box">
          <a href="/question">
            Pertanyaan Lainnya
          </a>
        </li>
        <li className="border-box">
          <a href="https://gitlab.com/darmaji.code.v/interview-question-jari.git" target="_blank">
            Get Source Code
          </a>
        </li>
      </ol>
    </div>
  </nav>)
}

export default Navbar