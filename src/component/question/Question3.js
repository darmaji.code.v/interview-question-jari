import React, { useRef, useState, useEffect, useMemo, ReactFragment } from "react"
import CONSTANTS from "../../constant"

const Question3 = () => {
  const [data, setData] = useState([])
  const [inputValue, setInputValue] = useState('')
  const [charMaxLength, setCharMaxLength] = useState({});

  const handleChange = (el) => {
    const value = el.target.value.replace(/ /g, '')
    setInputValue(value)

    groupingValue(value)
  }

  const groupingValue = (value) => {
    console.clear()

    const values = [...value]

    const groupValues = []
    let lastIndex = 0;
    values.reduce((prev, curr, index) => {
      const newIndex = !!groupValues?.length ? groupValues?.length : 0

      if (prev === 0) {
        groupValues[newIndex] = [curr]
        return curr
      }

      if (prev !== curr) {
        const futureValue = values[index+1] // mengambil value yg posisinya 1 index didepan
              
        if (curr === futureValue) {
          groupValues[newIndex] = [curr]
          lastIndex = newIndex
          return curr
        }

        groupValues[lastIndex] = [...groupValues[lastIndex], curr]
        return curr
      }

      if (prev === curr) {
        const futureValue = values[index+1] // mengambil value yg posisinya 1 index didepan        
        groupValues[lastIndex] = [...groupValues[lastIndex], curr]

        if (curr !== futureValue) {
          groupValues[newIndex] = []
          lastIndex = newIndex
          // return curr
        }

        return curr
      }

      return curr
    }, 0)

    console.clear()

    const removeEmptyGroupValues = groupValues?.filter(itemGoupValues => !!itemGoupValues?.length)
    const getCharMaxLength = removeEmptyGroupValues?.reduce((prev, curr, index) => {
      if (prev === 0) {
        return {
          index: index,
          length: curr.length
        }
      }

      return {
        index: (prev.length > curr.length) ? prev.index : index,
        length: (prev.length > curr.length) ? prev.length : curr.length
      }
    }, 0)

    setData([...removeEmptyGroupValues])
    setCharMaxLength({ ...getCharMaxLength })
  }

  const renderAnswerPosition = (answerPosition) => answerPosition.map((itemAnswerPosition, index) => (<div 
    className={index === charMaxLength.index ? 'answer-position active' : 'answer-position'} 
    key={index}
  >
    <div>
      {itemAnswerPosition}
    </div>
    <span className="small">
      {itemAnswerPosition.length}
    </span>
  </div>))

  const renderAnswer = useMemo(() => {


    if (!data.length) return

    const answerText = data[charMaxLength.index].join('')
    const answerPosition = data.map(itemAnswerPosition => itemAnswerPosition.join(''))




    return (<div>
      <div className="answer-position-wrapper">
        {renderAnswerPosition(answerPosition)}
      </div>
      <table>
        <tbody>
          <tr>
            <td valign="top">
              Jawaban
            </td>
            <td valign="top">:</td>
            <td>
              <b>{charMaxLength.length}</b> substring
            </td>
          </tr>
          <tr>
            <td valign="top">
              Penjelasan
            </td>
            <td valign="top">:</td>
            <td>
              Substring terpanjang adalah <b><i>{answerText}</i></b> dengan panjang {charMaxLength.length} substring
            </td>
          </tr>
        </tbody>
      </table>
    </div>)
  }, [data])

  return (
    <div>
      <div className="input-wrapper">
        <input 
          type="text" 
          className="input-text" 
          placeholder="Masukkan text . . ." 
          onChange={(el) => handleChange(el)} 
          value={inputValue}
        />
        <span className="small">
          Kamu dapat memasukkan text apapun selain spasi
        </span>
      </div>
      <div className="question-table-wrapper">
        {renderAnswer}
      </div>
    </div>
  )
}

export default Question3