import React, { useRef, useState, useEffect, useMemo, ReactFragment } from "react"
import CONSTANTS from "../../constant"

const Question2 = () => {
  const defaultFibonacci = 3
  let startPoint = [0, 1]
  const [data, setData] = useState([])
  const [inputValue, setInputValue] = useState('');

  const handleChange = (el) => {
    setTimeout(() => {
      const values = inputValue.split(',').map(item => {
        return (item !== '' && item < 3) ? defaultFibonacci : item
      })
      setInputValue(values.toString())

      generateFibonacci()
    }, 100);
  }

  const handleKeyDown = (el) => {
    const enableKey = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 188] // angka 0...9 dan koma (,)
    const status = !!enableKey.filter(itemKey => itemKey === el.keyCode).length

    if (el.keyCode === 8 && !!inputValue.length) { // kalo backspace dn valuenya ga kosong maka hapus satu
      setInputValue(inputValue.slice(0, inputValue.length-1))
      setData([])
      return
    }

    setInputValue(status ? inputValue+el.key : inputValue)
  }

  const generateFibonacci = () => {
    const values = inputValue.split(',').map(item => item.replace(/ /g, '')) // ambil value lalu di jadiin array trus ngapus spasi

    const newData = []

    values.map((itemValue, index) => {
      const newItemValue = parseInt(itemValue)
      if (newItemValue) {
        newData[index] = [...startPoint]

        do {
          const pushData = newData[index].slice(newData[index].length-2, newData[index].length).reduce((prev, curr) => prev+curr, 0) // ambil 2 array terakhir
          newData[index] = [...newData[index], pushData]
        } while (newData[index].length < newItemValue);
      }
    })

    setData([...newData])
  }

  const renderAnswer = useMemo(() => data.map((item, index) => (<li className="fibonacci-box word-wrap-break" key={index}>
    <span>
      Fibonacci - {item?.length}
    </span>
    <div>
      {item?.join(', ')}
    </div>
  </li>)), [data])

  return (
    <div>
      <div className="input-wrapper">
        <input 
          type="text"  
          className="input-text" 
          placeholder="Masukkan angka . . ." 
          onChange={(el) => handleChange(el)} 
          onKeyDown={(el) => handleKeyDown(el)}
          value={inputValue}
        />
        <span className="small">
          Kamu dapat memasukkan lebih dari 1 kombinasi angka dengan pemisah tanda koma (,). Contoh "7,9,25"
        </span>
      </div>
      <ol className="fibonacci-wrapper">
        {renderAnswer}
      </ol>
    </div>
  )
}

export default Question2