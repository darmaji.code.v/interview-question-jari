import React, { useRef, useState, useEffect, useMemo, ReactFragment } from "react"
import CONSTANTS from "../../constant"

const Question1 = () => {
  const [data, setData] = useState([])
  const inputHook = useRef()

  const { TABLE_DUMMY } = CONSTANTS

  const renderChildren = (dataChild) => dataChild.map((itemChild, index) => <tr key={index}>
    <td valign="top">
      {index+1}. {itemChild.name}
    </td>
  </tr>)

  const renderAnswer = useMemo(() => data.map((item, index) => {
    return (<table cellSpacing={0} border={1} className="table-question-1" key={index}>
      <thead>
        <tr className="code">
          <td align="center">
            {item.code}
          </td>
        </tr>
      </thead>
      <tbody>
        {renderChildren(item.children)}
      </tbody>
    </table>)
  }), [data])

  const handleChange = () => {
    const values = inputHook.current.value.replace(/ /g, '').split(',')

    const data = values.map(value => {
      
      const getData = TABLE_DUMMY.filter(item => item?.code?.toLocaleLowerCase() === value?.toLocaleLowerCase())
        .map(item => {
          const getChildData = TABLE_DUMMY.filter(itemChild => item?.code?.toLocaleLowerCase() === itemChild?.parent?.toLocaleLowerCase())

          return {
            ...item,
            children: getChildData
          }
        })

      return getData
    })
    .reduce((prev, curr) => [...prev, ...curr])

    setData([...data])
  }

  return (
    <div>
      <div className="input-wrapper">
        <input 
          type="text" 
          ref={inputHook} 
          onChange={() => handleChange()} 
          className="input-text" 
          placeholder="Masukkan kode . . ." 
        />
        <span className="small">
          Kamu dapat memasukkan lebih dari 1 kode dengan pemisah tanda koma (,). Contoh "A001,A002,A005"
        </span>
      </div>
      <div className="question-table-wrapper">
        {renderAnswer}
      </div>
    </div>
  )
}

export default Question1