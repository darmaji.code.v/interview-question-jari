import React, { useRef, useState, useEffect, useMemo, ReactFragment } from "react"
import CONSTANTS from "../../constant"

import Question1 from "./Question1"
import Question2 from "./Question2"
import Question3 from "./Question3"

const Question = () => {
  return (
    <div className="question-wrapper">
      <ol>
        <li>
          <div className="title-question">
            Pertanyaan Algoritme
          </div>
          <Question1 />
        </li>
        <li>
          <div className="title-question">
            Bilangan Fibonacci
          </div>
          <Question2 />
        </li>
        <li>
          <div className="title-question">
            Substring terpanjang tanpa duplikat
          </div>
          <Question3 />
        </li>
      </ol>
    </div>
  )
}

export default Question
