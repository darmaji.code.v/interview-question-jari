export const TABLE_DUMMY = [
  {
    code: "A001",
    name: "Wati",
    parent: null
  },
  {
    code: "A002",
    name: "Wira",
    parent: "A001"
  },
  {
    code: "A003",
    name: "Andi",
    parent: "A002"
  },
  {
    code: "A004",
    name: "Bagus",
    parent: "A002"
  },
  {
    code: "A005",
    name: "Siti",
    parent: "A001"
  },
  {
    code: "A006",
    name: "Hasan",
    parent: "A005"
  },
  {
    code: "A007",
    name: "Abdul",
    parent: "A006"
  },
]