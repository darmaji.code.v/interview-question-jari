import { POKEMON } from "./api";
import { TABLE_DUMMY } from "./data";

const CONSTANTS = {
  POKEMON,
  TABLE_DUMMY
}

export default CONSTANTS
