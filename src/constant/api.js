const baseUrl = 'https://api.pokemontcg.io'

export const POKEMON = {
  CARD: `${baseUrl}/v2/cards`,
  CARD_BY_ID: `${baseUrl}/v2/cards/`,
  TYPE: `${baseUrl}/v2/types`
}
