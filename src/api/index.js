import axios from "axios";
import CONSTANTS from "../constant";

export const CALL_POKEMON = async (pokemons) => {
  await axios.get(CONSTANTS.POKEMON.CARD, {
    'Access-Control-Allow-Origin' : '*',
    'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS',
  })
    .then(ress => pokemons = {...ress})
    .catch(err => pokemons = {...err})
  
  return pokemons
}
